var api = {

  ob_token: function() {
    return btoa($('#obUsername').val() + ":" + $('#obPassword').val());
  },

  getVersion: function() {
    $.ajax({
      url: "/api/",
      cache: false
    }).done(function(text) {
      alert(text);
    });
  },

  doAuth: function() {
    $.ajax({
      beforeSend: function (xhr) { xhr.setRequestHeader ("Authorization", "Basic " + api.ob_token()); },
      url: "/api/auth/",
      cache: false
    }).done(function( text ) {
      controller.authenticated();
    });
  },

  checkLive: function(onAirView, offAirView) {
    $.ajax({
      url: "/api/live/",
      cache: false
    }).done(function(text) {
      if (text == "On-Air") {
        onAirView();
      } else {
        offAirView();
      }
    });
  },

  startLive: function() {
    $.ajax({
      url: "/api/live/start/",
      cache: false
    });
  },

  postBroadcastInfo: function(show) {
    $.ajax({
      type: "POST",
      url: "/api/broadcast_info/",
      dataType: 'json',
      contentType: "application/json",
      data: JSON.stringify({status: 'On air', title: show.title, slug: show.slug, link: show.link, pic: show.pic }),
      cache: false
    });
  },

  stopLive: function() {
    $.ajax({
      url: "/api/live/stop/",
      cache: false
    });
  },

  getCurrentSong: function() {
    $.ajax({
      url: "/api/current_track/",
      dataType: "json",
      cache: false
    }).done(function(data) {
      ui.updateCurrent(data);
    });
  },

  setCurrentSong: function(meta_data) {
    $.ajax({
      beforeSend: function (xhr) { xhr.setRequestHeader ("Authorization", "Basic " + api.ob_token()); },
      url: "/api/current_track/",
      type:  "post",
      dataType: "json",
      contentType: "application/json",
      data:  JSON.stringify(meta_data),
      cache: false
    }).done(function(data) {
      ui.clearForm();
      api.getCurrentSong();
    });
  },

  fetchShows: function(email, callback) {
    $.ajax({
      url: "https://freshair.org.uk/api/users/" + email + "/shows/",
      // url: 'http://localhost/broadcast_controls/dummy.json',
      dataType: "json",
      cache: false
    }).done(function(data) {
      callback(data);
    });
  },

  fetchMessages: function() {
    $.ajax({
      url: "/api/messages/",
      dataType: "json",
      cache: false
    }).done(function(data) {
      controller.updateMessages(data);
    });
  },

  checkBroadcastTime: function(show, time, callback) {
    $.ajax({
      url: "https://freshair.org.uk/api/shows/" + show + '/' + 'check-broadcast-time/' + time,
      dataType: "text",
      cache: false
    }).done(function(data) {
      callback(data, time);
    });
  },

  checkBroadcastTimes: function(show, start, end, callback) {
    $.ajax({
      url: "https://freshair.org.uk/api/shows/" + show + '/' + 'check-broadcast-time/' + moment(start).unix() + '-' + moment(end).unix(),
      dataType: "text",
      cache: false
    }).done(function(data) {
      callback(data);
    });
  },

  fetchLoginRecords: function(startTime, endTime, callback) {
    $.ajax({
      url: "/api/login_records/?start=" + startTime + "&end=" + endTime,
      dataType: "text",
      cache: false
    }).done(function(data) {
      callback(data);
    });
  }
}
