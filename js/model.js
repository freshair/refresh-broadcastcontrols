let model = {
  shows: {},
  selectedShow: null,
  broadcastTime: null,
  isAuth: false,

  registerShows: function(shows) {
    this.shows = {};
    for (let i = 0; i < shows.length; i++) {
      this.shows[shows[i].slug] = {
        title: shows[i].title,
        slug: shows[i].slug,
        pic: shows[i].pic,
        link: shows[i].link
      };
    }
  },

  selectShow: function(slug) {
    this.selectedShow = slug;
  },

  clearSelectedShow: function() {
    this.selectedShow = null;
  },

  authenticated: function() {
    this.isAuth = true;
  }

}
