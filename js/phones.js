var phones = {
  get_status: function () {
    $.get('https://asterisk.freshair.org.uk/phonestatus.php')
      .done(function(data) {
        $('#phone_status .status').text(data);

        if (data == 'Studio Phones') {
          $('#phone_status').addClass('studio');
        } else {
          $('#phone_status').removeClass('studio');
        }
      });
  }
}
