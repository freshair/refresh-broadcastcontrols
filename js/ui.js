var ui = {
    authenticated: function() {
        $('#song_form input').removeAttr('disabled')
        $('#song_form .submit').show()
        $('#song_form .clear').show()
        $('#show_ob_auth').hide()
        $('#ob_auth').slideUp()

        $('#song_form .clear').click(ui.clearForm);
        $('#song_form').submit(function(e) {
          e.preventDefault();
          ui.sendSong();
        });
    },

    isLive: function() {
        $('#live_button').removeClass('offair').addClass('onair');
        $('#live_button .status').html('Live');
        $('#live_button').off();
        $('#live_button').click(function(e){
            e.preventDefault();
            if (controller.isAuth()) {
                $('#live_button .status').html('Stopping...');
                controller.stopLive();
            }
        });
        if (controller.isAuth()) {
            $('#songform').show();
        }
    },

    notLive: function() {
        $('#live_button').removeClass('onair').addClass('offair');
        $('#live_button .status').html('Off-Air');
        $('#live_button').off();
        $('#live_button').click(function(e){
            e.preventDefault();
            if (controller.isAuth() && (controller.getSelectedShow() !== null)) {
                $('#live_button .status').html('Starting...');
                controller.goLive();
            }
        });
        $('#songform').hide();
    },

    updateCurrent: function(meta_data) {
        $('#song_form input').removeAttr('placeholder')

        $.each(meta_data, function (meta, data) {
            $('#song_form #input' + meta.capitalize()).attr('placeholder', data);
        });

        if (meta_data.error) {
            $('#song_form .error').html(meta_data.error).show();
        } else {
            $('#song_form .error').hide();
        }
    },

    sendSong: function() {
        meta_data = {}

        $('#song_form input').each(function (i, item) {
            meta_data[item.name] = item.value
        })

        api.setCurrentSong(meta_data);
    },

    clearForm: function () {
        $('#song_form input').val("");
    },

    updateWebcams: function() {
        $('#webcam1').attr('src', 'http://webcam.freshair.org.uk/1?' + Math.random());
        $('#webcam2').attr('src', 'http://webcam.freshair.org.uk/2?' + Math.random());
    },

    disableShows: function() {
      $('#show-selector').empty();
      $('#show-selector').append('<option value="dummy">Waiting for valid email address</option>');
      $('#show-selector').prop('disabled', true);
    },

    updateShows: function(shows) {
      if(Object.keys(shows).length === 0) {
          this.disableShows();
          return;
      }

      $('#show-selector').empty();

      for (let slug in shows) {
        if (shows.hasOwnProperty(slug)) {
          $('#show-selector').append('<option value="' + slug + '">' + shows[slug].title + '</option>');
          $('#show-selector').removeAttr('disabled');
        }
      }

    },

    logout: function() {
      $('#email-field').val('');
      $('#logged-in').hide();
      $('#logged-out').show();
      this.disableShows();
    },

    login: function(slug) {
      $('#logged-out').hide();
      $('#logged-in').show();
      document.getElementById('login-message').innerHTML = "You're logged in as " + controller.getShow(slug).title;
    },

    updateMessages: function(messages) {
      let html = '';
      if (messages.length > 0) {
        let message = {};
        for (let i = 0; i < messages.length; i++) {
          message = JSON.parse(messages[i]);
          html += '<div class="message">';
          html += '<em>' + message.author + ' at ' + message.time + ' on ' + message.date ;
          html += '</em>';
          html += '<p>' + message.content;
          html += '</p>';
          html += '</div>';
        }
      } else {
        html = '<em>No message.</em>'
      }

      $('#incoming-messages').html(html);
    },

    uploadStepOne: function() {
      $('#upload-step-one').show();
      $('#upload-step-two').hide();
      $('#upload-step-three').hide();

      $('#upload-step-one-btn').click(function() {
        let slug = document.getElementById('show-selector').value;
        controller.uploadStepTwo(slug);
      });
    },

    uploadStepTwo: function(slug) {
      $('#upload-step-one').hide();
      $('#upload-step-two').show();
      document.getElementById('upload-step-two-msg').innerHTML =
        "You selected show " + controller.getShow(slug).title + '.<br>Select a broadcast time';
      $('#upload-step-three').hide();

      $('#upload-step-two-btn-back').click(function() {
        controller.uploadStepOne();
      });

      $('#upload-step-two-btn').click(function() {
        show = document.getElementById('show-selector').value;
        broadcastTime = document.getElementById('broadcast-time').value;
        controller.uploadCheckStepTwo(show, broadcastTime);
      });
    },

    uploadStepThree: function() {
      $('#upload-step-one').hide();
      $('#upload-step-two').hide();
      $('#upload-step-three').show();

      $('#upload-step-three-btn-back').click(function() {
        show = document.getElementById('show-selector').value;
        controller.uploadStepTwo(show);
      });
    },

    updateFileName: function(fileName) {
      $('#filename').val(fileName);
    },

    insertLoginRecords: function(records) {
      records = JSON.parse(records);
      let recordsElement = document.getElementById('records');
      let html = '';
      let time;

      for (var i = 0; i < records.length; i++) {
        time = moment.unix(records[i][1]).format("YYYY-MM-DD HH:mm");
        html += '<div class="show-record"> <div class="time-record">' + time + '</div>' + records[i][0] + "</div>";
      }

      recordsElement.innerHTML = html;
    }

}
