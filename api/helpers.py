# FreshAir Studio API
# Hayden Ball 2013

import re
import string

def handle_metadata(chunk):
    latest = re.search(r"--- 1 ---\n(.+)", chunk, re.MULTILINE|re.DOTALL).group(1)

    sections  = latest.split("\n")
    meta_data = map(lambda section: re.search(r"(.+?)=\"(.*)\"", section).groups(), sections)

    return dict(meta_data)