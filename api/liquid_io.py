from config import *
import os
import telnetlib

def liquid_io(name, content=None):
    response = ""

    if name == 'live':
        tn = telnetlib.Telnet('127.0.0.1', 1201)
    else:
        tn = telnetlib.Telnet('127.0.0.1', 1200)

    tn.write(content + "\n")
    response = tn.expect(["END"])
    tn.write("exit\n")

    response = response[2].rstrip("\r\nEND")

    return response
